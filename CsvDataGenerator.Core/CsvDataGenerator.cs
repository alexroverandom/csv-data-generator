﻿namespace CsvDataGenerator.Core
{
	using CsvHelper;
	using System;
	using System.Collections.Generic;
	using System.Globalization;
	using System.IO;

	/// <summary>
	/// Class to generate csv-content
	/// </summary>
	public class CsvDataGenerator
	{
		private const string CONTACT_NAME = "Contact{0}-{1}";
		private const string CONTACT_EMAIL = "email{0}.{1}@test.com";
		private const string CONTACT_PHONE = "+38{0}{1}";
		private const string CONTACT_VISITOR = "ac{0}{1}";

		public IEnumerable<ContactModel> GenerateData(int count) {
			var date = DateTime.UtcNow;
			var result = new List<ContactModel>();
			for (int i = 0; i < count; i++) {
				var model = new ContactModel {
					CONTACT_NAME = string.Format(CONTACT_NAME, date.ToString("HHmmss"), i),
					CONTACT_EMAIL = string.Format(CONTACT_EMAIL, date.ToString("HHmmss"), i),
					CONTACT_PHONE = string.Format(CONTACT_PHONE, date.Second.ToString("D2"), i.ToString("D8")),
					CONTACT_VISITOR_ID = string.Format(CONTACT_VISITOR, date.ToString("HHmmss"), i.ToString("D8"))
				};
				result.Add(model);
			}
			return result;
		}

		public bool WriteData(string path, IEnumerable<ContactModel> data) {
			if (!File.Exists(path)) {
				using (FileStream fs = File.Create(path)) { }
			}
			using (var writer = new StreamWriter(path))
			using (var csv = new CsvWriter(writer, CultureInfo.InvariantCulture)) {
				csv.WriteRecords(data);
			}
			return true;
		}

		public bool Generate(CsvDataGeneratorConfig config) {
			var data = GenerateData(config.Count);
			return WriteData(config.Path, data);
		}
	}
}
