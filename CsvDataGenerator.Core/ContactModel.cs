﻿namespace CsvDataGenerator.Core
{
	public class ContactModel
	{
		public string CONTACT_NAME { get; set; }
		public string CONTACT_EMAIL { get; set; }
		public string CONTACT_PHONE { get; set; }
		public string CONTACT_VISITOR_ID { get; set; }

	}
}
