﻿namespace CsvDataGenerator.Core
{
	public class CsvDataGeneratorConfig
	{

		public int Count { get; set; }
		public string Path { get; set; }

	}
}
