﻿namespace CsvDataGenerator
{
	using System;
	using CsvDataGenerator.Core;

	class Program
	{
		private static bool Generate() {
			Console.WriteLine("Type .csv file path...");
			var path = Console.ReadLine();
			Console.WriteLine("Type records count...");
			var countStr = Console.ReadLine();
			if (int.TryParse(countStr, out var count)) {
				Console.WriteLine($"Generate for {count} records...");
				var generator = new CsvDataGenerator();
				return generator.Generate(new CsvDataGeneratorConfig {
					Count = count,
					Path = path
				});
			} else {
				Console.WriteLine("Not valid typed count.");
				return false;
			}
		}

		private static void TryGenerate() {
			if (Generate()) {
				Console.WriteLine("Done!");
				Console.ReadKey();
			} else {
				Console.WriteLine("Some error is happend. Please try again...");
				Console.WriteLine("Press 0 to exit...");
				var res = Console.ReadLine();
				if (res == "0") {
					return;
				}
				TryGenerate();
			}
		}

		static void Main(string[] args) {
			TryGenerate();
		}
	}
}
